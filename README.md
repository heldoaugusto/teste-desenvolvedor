![Umobi](https://umobi.com.br/images/svg/logo-color.svg)

> Esse teste é público. Todos os interessados que o fizerem receberão um feedback da nossa equipe. 

## Teste para candidatos à vaga de desenvolvedor

Somos um time de desenvolvimento de Software com múltiplas expertises e vários projetos em manutenção e desenvolvimento contínuo. Nosso portfólio pode ser encontrado no link:
[Umobi](https://umobi.com.br/portfolio)

## Quais tecnologias utilizamos?

Nosso time é composto por desenvolvedores com mútiplas especialidades

* Angular
* Ionic
* PHP
* Laravel
* Swift
* Objective C
* Java
* Kotlin
* Unix/Linux

Você não é obrigado a conhecer todas as tecnologias acima mas é um diferencial ter o mínimo de experiência em algum dos frameworks citados.

*Vamos ao teste!*

> Seus objetivos são: #1- Criar uma api em PHP com 2 endpoints, 1 endpoint responsável pelo cadastro de clientes e outro endpoint responsável pela listagem com possibilidade de busca. #2- Criar um APP Android ou iOS: iOS em React Native ou Nativo, Android em Java ou Kotlin, o app deve ter duas telas, uma que exibe um formulário e outra que exibe a lista de clientes.

## Instruções para o teste:

* API
1. Faça um fork deste repositório;
2. A pasta para o código da API é a pasta **api**;
3. A api pode utilizar o banco de dados da sua escolha, preferencialmente MySql.
4. Preocupe-se em desenvolver utilizando boas práticas;

* App
1. Faça um fork deste repositório;
2. A pasta para o código do aplicativo é a pasta **app**;

### Leia, entenda, e tire todas as dúvidas referentes ao teste [aqui](https://bitbucket.org/umobi/teste-desenvolvedor/issues?status=new&status=open). 

> Faça uma estimativa de horas para o desenvolvimento e envie um email com o título [Teste Dev] Estimativa para contato@umobi.com.br
>
> Quando finalizar o teste, publique tudo no seu Bitbucket ou Github e envie um email com o título [Teste Dev] Finalizado para contato@umobi.com.br

### Wireframe
Você fará o desenvolvimento do App seguindo os wireframes abaixo

#### Lista de Clientes
![Listagem de Clientes](https://i.imgur.com/yLLNkJc.jpg)

#### Cadastro de Clientes
![Listagem de Clientes](https://i.imgur.com/Lo04pro.jpg)

#### Detalhes de Clientes
![Listagem de Clientes](https://i.imgur.com/Pru1wUd.jpg)


> **Importante:** Usamos o mesmo teste para todos os níveis de desenvolvedor: **trainee**, **junior**, **pleno** ou **senior**, mas procuramos adequar nossa exigência na avaliação com cada um desses níveis sem, por exemplo, exigir excelência de quem está começando :-D

### Ganhe Bônus
* Utilize laravel para desenvolver a API.
* Implemente na API, alguma forma de autenticação, para que somente usuários autenticados possam cadastrar clientes.
* Utilize o padrão RESTFul para o desenvolvimento da API.
* Utilize algum padrão de resposta JSON.

* Desenvolva o aplicativo utilizando linguagem Nativa.
* Implemente algum tipo de paginação por scroll.

**Boa sorte!**